import { test } from '@playwright/test';
import { LoginPage } from '../pages/login-page';

let loginPage: LoginPage;

test.beforeEach(async ({ page }) => {
    loginPage = new LoginPage(page);
    await loginPage.go();
});

test('Deve realizar login com sucesso', async () => {
    await loginPage.sigIn('qa', 'cademy');
    await loginPage.userLoggedIn('Sua credenciais são validas :)');
});

test('Deve validar senha incorreta', async () => {
    await loginPage.sigIn('qa', 'abc123');
    await loginPage.toastMessage('Oops! Credenciais inválidas :(');
});

test('Deve validar usuário obrigatório', async () => {
    await loginPage.sigIn('', 'cademy');
    await loginPage.toastMessage('Informe o seu nome de usuário!');
});

test('Deve validar senha obrigatória', async () => {
    await loginPage.sigIn('qa', '');
    await loginPage.toastMessage('Informe a sua senha secreta!');
});
